<?php require_once('Connections/paginaciondb.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['del'])) && ($_GET['del'] != "")) {
  $deleteSQL = sprintf("DELETE FROM sitio_web WHERE id_sitio=%s",
                       GetSQLValueString($_GET['del'], "int"));

  mysql_select_db($database_paginaciondb, $paginaciondb);
  $Result1 = mysql_query($deleteSQL, $paginaciondb) or die(mysql_error());
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
		$Foto = $_FILES['imagen_sitio']['name'];
  $insertSQL = sprintf("INSERT INTO sitio_web (nombre_sitio, enlace_sitio, imagen_sitio) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['nombre_sitio'], "text"),
                       GetSQLValueString($_POST['enlace_sitio'], "text"),
                       GetSQLValueString($Foto, "text"));

  mysql_select_db($database_paginaciondb, $paginaciondb);
  $Result1 = mysql_query($insertSQL, $paginaciondb) or die(mysql_error());
  
  if($Result1){
	 move_uploaded_file($_FILES['imagen_sitio']['tmp_name'],"sitios/".$Foto);
	  
  }

  $insertGoTo = "sitio.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_paginaciondb, $paginaciondb);
$query_Recordset1 = "SELECT * FROM sitio_web";
$Recordset1 = mysql_query($query_Recordset1, $paginaciondb) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Agregar Sitio Web</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap-social.css">
</head>
<body style="background:#263238;">

<div class="container">
<h1 class="center pink-text">Ingresar Sitio</h1>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>" enctype="multipart/form-data">
      <div class="input-field col s12 m6">
       <input id="nombre" type="text" name="nombre_sitio" >
         <label for="nombre">Nombre</label>
     </div>
  <div class="input-field col s12 m6">
          <input id="enlace" type="text" name="enlace_sitio" >
           <label for="enlace">Enlace</label>
    </div>
     <div class="input-field col s12 m6">
          <div class="file-field input-field">
      <div class="btn">
        <span>File</span>
        <input name="imagen_sitio" id="enlace" type="file">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
    </div>
      <br><br>
    <input class="btn orange waves-effect waves-light hoverable" type="submit" value="Insertar registro"> 
      <input type="hidden" name="MM_insert" value="form1">
  </form>
  
  <?php if(isset($_GET['del']) && !empty($_GET['del'])){ ?>
  <br><br>
	<div class="chip green darken-1 white-text" >
        Registro borrado correctamente
        <i class="material-icons">close</i>
      </div>
       
    <?php } ?>
     <table class="white">
       <thead>
         <tr>
           <th data-field="id">Nombre</th>
           <th data-field="name">Direccion</th>
           <th data-field="price">Acción</th>
         </tr>
       </thead>
        <?php do { ?>
       <tbody>
         <tr>
           <td><?php echo $row_Recordset1['nombre_sitio']; ?></td>
           <td><?php echo $row_Recordset1['enlace_sitio']; ?></td>
           <td><i style="color:red;" class="medium material-icons"><a href="sitio.php?del=<?php echo $row_Recordset1['id_sitio']; ?>">delete_forever</a></i></td>
         </tr>
        </tbody>
             <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
  </table>
 <br><br>
 <a class="btn btn-large  waves-effect waves-light  center" href="lobby.php">Volver</a>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/materialize.min.js"></script>
<script >
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();
    $('.modal-trigger').leanModal();

  });
</script>
 
        
</body>

</html>
<?php
mysql_free_result($Recordset1);
?>
