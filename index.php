
<!DOCTYPE html>
<html>
<head>
  <title>Venta de sitios web | Paginacion</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="Content-Language" content="es-ve" />
  <meta name="robots" content="index,follow" />
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
  <meta name="title" content="Venta de sitios web | Paginacion">
  <meta name="description" content="Quieres comprar páginas web, diseños impresionante, sitios a buen precio, facil de encontrar, con gran demanda y que puedas publicar lo que sea. ¿Que esperas? Contactanos.">
  <meta name="keywords" content="creacion de páginas, página web, comprar página web, venezuela, e-comerce, blog.">
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="fav/ico.png" />
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap-social.css">
</head>
<body>
 <nav class=" blue-grey darken-4 ">
    <div class="nav-wrapper container">
      <a href="index.php" class="brand-logo "><img style="margin-top:2px;" src="img/logo.png" width="200px" height="60x"></a>
      <ul id="nav-mobile" class="right  hide-on-med-and-down">
        <li class="active" ><a href="index.php">Inicio</a></li>
        <li><a href="portafolio.php">Portafolio</a></li>
        <li data-target="modal1" class="modal-trigger"><a href="#">Contacto</a></li>
      </ul>
     <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
     <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php">Inicio</a></li>
        <li><a href="portafolio.php">Portafolio</a></li>
        <li data-target="modal1" class="modal-trigger"><a href="#">Contacto</a></li>
      </ul>
    </div>
  </nav>

<div class="hide-on-small-only parallax-container white-text lighten-2-text">
    <div class="parallax"><img src="img/img1.jpg"></div>
      <h1 class=" center-align white-text lighten-2-text ">Desarrollo de Sitios Web</h1>
      <div class="row container">
        <div class="col m6 white-text center-align">
        <i class="large material-icons">people</i>
        <h3>¿Quienes somos?</h3>
              <p>Paginacion es una empresa venezolana, nacida para diseñar su 
              sitio web con las especificaciones de su preferencia. Contamos con 
              un equipo de profesionales en desarrollo web y diseño de sistemas de 
              información automatizados, listos para convertir la página de sus sueños en realidad.</p>
        </div>
        <div class="col m6 white-text center-align">
        <i class="large material-icons">devices</i>
        <h3>¿Porqué internet?</h3>
            <p>En la actualidad el 68% de las personas usan internet para informarse sobre un servicio o producto antes de adquirirlo. Un sitio web le posibilita a su empresa tener:</p>      
            <p>-Alcance mundial.</p>
           <p> -Garantía de su prestigio y calidad.</p>
            <p>-Tarjeta de presentación.</p>
           
      </div>
    </div>
  </div>
<div class="section withe">
   
     <div class="row">
     <br><br><br><br><br><br>
        <div class="col s6 m6 l2">
          <center><div class=" rad centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">send</i><br><span class="light letra">Atencion rapida</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">favorite</i><br><span class="light letra">Buen trato</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">phone</i><br><span class="light letra">Contacto continuo</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">done_all</i><br><span class="light letra">Trabajo de calidad</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">alarm_on</i><br><span class="light letra">Tiempo real</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad centered blue-grey darken-4 white-text hoverable"><i class="fa fa-money large"></i><br><span class="light letra">Mejor precio</span></div></center>
        </div>
    </div>  
</div>
 
<div class="section cyan darken-2">
       <div class="row container">

   <div class="col m4 s12 white-text center-align">
    <div class="title col-md-12 white-text"><h4><span class="glyphicon glyphicon-blackboard"></span> Presentación</h4></div>
        <p> Diseño temático a pedido.</p>
        <div class="divider"></div>
         <p>Informacion completa de su empresa</p>
         <div class="divider"></div>
         <p>Dominio .com.ve por un año.</p>
         <div class="divider"></div>
         <p>Alojamiento mínimo.</p>
         <div class="divider"></div>
         <p>Posicionamiento básico en SEO.</p> <br><br>
      <a href="pagina-web.php" class="waves-effect waves-light btn btn blue-grey darken-4 centered"><i class="material-icons left">info</i> Más información</a>
        </div>
      <div class="col m4 s12 white-text center-align">
      <div class="title col-md-12 white-text"><h4><span class="glyphicon glyphicon-blackboard"></span> Blog/Noticia</h4></div>
        <p> Aplicación de publicaciones personalizadas.</p>
        <div class="divider"></div>
         <p>Diseño adaptable a moviles.</p>
         <div class="divider"></div>
         <p>Dominio .com.ve por un año.</p>
         <div class="divider"></div>
         <p>Alojamiento mínimo de 10Gb.</p>
         <div class="divider"></div>
         <p>Posicionamiento en SEO.</p> <br><br>
       <a href="blog.php" class="waves-effect waves-light btn btn blue-grey darken-4 centered"><i class="material-icons left">info</i> Más información</a>
      </div>
      <div class="col m4 s12 white-text center-align">
      <div class="title col-md-12 white-textblue-grey-text darken-4-text"><h4><span class="glyphicon glyphicon-blackboard"></span> E-Comerce </h4></div>
        <p> Aplicacion de carrito de compras.</p>
        <div class="divider"></div>
         <p>Diseño adaptable a moviles.</p>
         <div class="divider"></div>
         <p>Dominio .com.ve por un año.</p>
         <div class="divider"></div>
         <p>Alojamiento mínimo de 10Gb.</p>
         <div class="divider"></div>
         <p>Posicionamiento en SEO.</p> <br><br>
         <a href="e-comerce.php" class="waves-effect waves-light btn btn blue-grey darken-4 centered"><i class="material-icons left">info</i> Más información</a>
      </div>
    </div>
</div>

<footer style="margin-top:0;" class="page-footer blue-grey darken-4">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Desarrollo web </h5>
                <p class="grey-text text-lighten-4">Paginacion.com.ve</p>
                <button data-target="modal1" class="btn btn-large orange accent-3 modal-trigger waves-effect waves-light hoverable"><i class="material-icons large left">email</i>Contactanos</button>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Siguenos:</h5>
                <ul>
                  <li><a href="https://www.facebook.com/Paginacion/" class="btn btn-block btn-social btn-facebook"><span class="fa fa-facebook"></span>Facebook</a></li><br>
                  <li><a href="https://twitter.com/Paginacion" class="btn btn-block btn-social btn-twitter"><span class="fa fa-twitter"></span>Twitter</a></li><br>
                  <li><a href="#" class="btn btn-block btn-md btn-social btn-github"><span class="fa fa-github"></span>Github</a></li><br>
                  <li><a href="https://plus.google.com/u/2/106363738317204293945" class="btn btn-block btn-md btn-social btn-google"><span class="fa fa-google"></span>Google+</a></li><br>
                </ul>
              </div>
            </div>
          </div>

          <div class="footer-copyright">
            <div class="container">
            © 2016 Copyright Paginacion.com.ve
            </div>
          </div>   
</footer>
  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">  
      <div class="row ">
      
    <form class="col s12" action="email.php" method="post" name="frm">
    <div class="col s6">
      <div class="row">
      <h4 class="hide-on-small-only ">Envíanos un Mail</h4>
        <div class="input-field col s12 m6">
          <input  id="nombre" name="nombre" type="text" class="validate" required>
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input  id="apellido" name="apellido" type="text" class="validate" required>
          <label for="apellido">Apellido</label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s12 m6">
            <input id="telefono" name="telefono" type="number" class="validate" required>
            <label for="telfono">Telefono</label>
          </div>
          <div class="input-field col s12 m6">
            <input id="email" name="email" type="email" class="validate" required>
            <label for="email">Email</label>
          </div>
          <button type="submit"  class="btn waves-effect waves-light "><i class="hide-on-small-only  material-icons right">send</i>Enviar</button>
      </div>
      </div>
        <div class="input-field col s12 m6" style="border-left: 2px solid #00acc1;">
            <textarea id="textarea1" style="height: 145px;" name="mensaje"class=" materialize-textarea"  length="255" maxlength="255"></textarea>
            <label for="textarea1">Mensaje</label>
          </div>
     
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn red">Cerrar</a>
    </div>
  </div>
 </div>

<script src="js/jquery.min.js"></script>
<script src="js/materialize.min.js"></script>
<script >
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();
    $('.modal-trigger').leanModal();

  });
</script>
 
        
</body>

</html>