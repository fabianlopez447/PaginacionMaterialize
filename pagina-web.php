<!DOCTYPE html>
<html>
<head>
  <title>Plan Web Basica | Paginacion</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="Content-Language" content="es-ve" />
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
  <meta name="title" content="Plan Web Basica | Paginacion">
  <meta name="description" content="Quieres comprar páginas web, diseños impresionante, sitios a buen precio, facil de encontrar, con gran demanda y que puedas publicar lo que sea. ¿Que esperas? Contactanos.">
  <meta name="keywords" content="creacion de páginas, página web, comprar página web, venezuela, e-comerce, tienda online, crear tienda virtual">
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="fav/ico.png" />
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap-social.css">
</head>
<body>
 <nav class="blue-grey darken-4">
    <div class="nav-wrapper container">
      <a href="index.php" class="brand-logo "><img style="margin-top:2px;" src="img/logo.png" width="200px" height="60x"></a>
      <ul id="nav-mobile" class="right  hide-on-med-and-down">
        <li><a href="index.php">Inicio</a></li>
        <li class="active" ><a  href="portafolio.php">Portafolio</a></li>
        <li data-target="modal1" class="modal-trigger"><a href="#">Contacto</a></li>
      </ul>
     <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
     <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php">Inicio</a></li>
        <li><a href="portafolio.php">Portafolio</a></li>
        <li data-target="modal1" class="modal-trigger"><a href="#">Contacto</a></li>
      </ul>
    </div>
  </nav>

<div class="row container">
<nav>
    <div class="nav-wrapper cyan darken-2">
      <a href="blog.php" class="brand-logo right"> Blog/Noticia ></a>
    </div>
  </nav>
      <h1><i style="color:#00bfa5;" class="medium material-icons">web</i> Página web</h1>
       <p><span style="font-size:18px;">P</span>erfecta como tarjeta de presentacion de tu empresa o negocio, este tipo de sitios son de gran ayuda para darse a conocer y ofrecer una información más detallada acerca de lo que hace tu empresa/negocio</p>
        <p><b>Como plan básico le ofrecemos lo siguiente:</b></p>
        <ul>
          <li><i style="color:#00bfa5;" class="fa fa-check"></i> Diseño completamente responsive</li>
          <li><i style="color:#00bfa5;" class="fa fa-check"></i> Consta de 3 páginas(Inicio, Información y Contacto)</li>
          <li><i style="color:#00bfa5;" class="fa fa-check"></i> Información completa de su empresa.</li>
          <li><i style="color:#00bfa5;" class="fa fa-check"></i> Dominio .com.ve por un año.</li>
          <li><i style="color:#00bfa5;" class="fa fa-check"></i> Alojamiento economico.</li>
        </ul>
        <p>Aparte de estos paquetes también incluimos el posicionamiento en buscadores(SEO) para asegurarse de que su sitio aparezca en los resultados del buscador Google.</p>
   <form  action="email2.php" method="post" name="service" class="col s12">
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header waves-effect waves-dark"><i class="material-icons">add_shopping_cart</i>Contratar servicio.</div>
            <div class="collapsible-body container">
            <br>
              <div class="input-field">
                <label for="">Nombres:</label>
                <input type="text" name="nombres" required>
              </div>
              <div class="input-field">
                <label for="">Apellidos:</label>
                <input type="text" name="apellidos" required>
              </div>
              <div class="input-field">
                <label for="">Telefono:</label>
                <input type="number" name="telefono" required>
              </div>
              <div class="input-field">
                <label for="">Correo electronico</label>
                <input type="email" name="email" required>
              </div>
              <div class="input-field">
                <label for=""><b>Plan/servicio:</b></label>
                <input type="text" value="Pagina Web" disabled>
                <input type="hidden" name="plan" value="Pagina Web" >
              </div>
              <div class="input-field">
                <button type="submit" class="btn">Contratar</button>
              </div><br><br>
            </div>
          </li>
        </ul>
    </form>
</div>

<div class="section cyan darken-2">
   
     <div class="row">
        <div class="col s6 m6 l2">
          <center><div class=" rad centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">send</i><br><span class="light letra">Atencion rapida</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">favorite</i><br><span class="light letra">Buen trato</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">phone</i><br><span class="light letra">Contacto continuo</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">done_all</i><br><span class="light letra">Trabajo de calidad</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad  centered blue-grey darken-4 white-text hoverable"><i class="material-icons large">alarm_on</i><br><span class="light letra">Tiempo real</span></div></center>
        </div>
        <div class="col s6 m6 l2">
          <center><div class=" rad centered blue-grey darken-4 white-text hoverable"><i class="fa fa-money large"></i><br><span class="light letra">Mejor precio</span></div></center>
        </div>
    </div>  
</div>

<footer style="margin-top:0;" class="page-footer blue-grey darken-4">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Desarrollo web </h5>
                <p class="grey-text text-lighten-4">Paginacion.com.ve</p>
                <button data-target="modal1" class="btn btn-large orange accent-3 modal-trigger waves-effect waves-light hoverable"><i class="material-icons large left">email</i>Contactanos</button>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Siguenos:</h5>
                <ul>
                  <li><a href="https://www.facebook.com/Paginacion/" class="btn btn-block btn-social btn-facebook"><span class="fa fa-facebook"></span>Facebook</a></li><br>
                  <li><a href="https://twitter.com/Paginacion" class="btn btn-block btn-social btn-twitter"><span class="fa fa-twitter"></span>Twitter</a></li><br>
                  <li><a href="#" class="btn btn-block btn-md btn-social btn-github"><span class="fa fa-github"></span>Github</a></li><br>
                  <li><a href="https://plus.google.com/u/2/106363738317204293945" class="btn btn-block btn-md btn-social btn-google"><span class="fa fa-google"></span>Google+</a></li><br>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2016 Copyright Paginacion.com.ve
            </div>
          </div>   
</footer>
 <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">  
      <div class="row ">
      
    <form class="col s12" action="email.php" method="post" name="frm">
    <div class="col s6">
      <div class="row">
      <h4 class="hide-on-small-only ">Envíanos un Mail</h4>
        <div class="input-field col s12 m6">
          <input  id="nombre" type="text" name="nombre"  class="validate" required>
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input  id="apellido" name="apellido" type="text" class="validate" required>
          <label for="apellido">Apellido</label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s12 m6">
            <input id="telefono" name="telefono" type="number" class="validate" required>
            <label for="telfono">Telefono</label>
          </div>
          <div class="input-field col s12 m6">
            <input id="email" name="email" type="email" class="validate" required>
            <label for="email">Email</label>
          </div>
          <button type="submit"  class="btn waves-effect waves-light "><i class="hide-on-small-only  material-icons right">send</i>Enviar</button>
      </div>
      </div>
        <div class="input-field col s12 m6" style="border-left: 2px solid #00acc1;">
            <textarea id="textarea1" style="height: 145px;" name="mensaje"class=" materialize-textarea"  length="255" maxlength="255"></textarea>
            <label for="textarea1">Mensaje</label>
          </div>
     
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn red">Cerrar</a>
    </div>
  </div>
 </div>

<script src="js/jquery.min.js"></script>
<script src="js/materialize.min.js"></script>
<script >
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();
    $('.modal-trigger').leanModal();

  });
</script>
 
        
</body>

</html>